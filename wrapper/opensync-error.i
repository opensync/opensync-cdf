typedef struct {} Error;
%feature("exceptionclass") Error;
%extend Error {

	%{
		static Error* errorwrap_new()
		{
			Error *error = calloc(1, sizeof(Error));
			error->ref = 1;
			return error;
		}

		static void errorwrap_ref(Error **error)
		{
			if( error && *error ) {
				(*error)->ref++;
			}
		}

		static void errorwrap_unref(Error **error)
		{
			if( error && *error ) {
				(*error)->ref--;
				if( (*error)->ref == 0 ) {
					free(*error);
					*error = NULL;
				}
			}
		}

		static PyObject* errorwrap_new_pyobject(OSyncError *oserr)
		{
			Error *error = errorwrap_new();
			error->error = oserr;
			osync_error_ref(&error->error);
			return SWIG_NewPointerObj(error, SWIGTYPE_p_Error,
				SWIG_POINTER_NEW);
		}
	%}

	/* called by python-module plugin */
	Error(PyObject *obj) {
		Error *error = errorwrap_new();
		Error *other = PyCObject_AsVoidPtr(obj);

		if (other) {
			error->error = other->error;
			osync_error_ref(&error->error);
		}

		return error;
	}

	Error(const char *msg, ErrorType type=OSYNC_ERROR_GENERIC) {
		Error *error = errorwrap_new();
		osync_error_set(&error->error, type, "%s", msg);
		return error;
	}

	~Error() {
		osync_error_unref(&self->error);
		errorwrap_unref(&self);
	}

	const char *get_name() {
		return osync_error_get_name(&self->error);
	}

	bool is_set() {
		return osync_error_is_set(&self->error);
	}

	ErrorType get_type() {
		return osync_error_get_type(&self->error);
	}

	void set_type(ErrorType type) {
		osync_error_set_type(&self->error, type);
	}

	const char *get_msg() { // 'print' is a reserved word
		return osync_error_print(&self->error);
	}

	char *print_stack() {
		return osync_error_print_stack(&self->error);
	}

	void set_from_error(Error *source) {
		osync_error_set_from_error(&self->error, &source->error);
	}

	void set_error(ErrorType type, const char *msg) {
		osync_error_set(&self->error, type, "%s", msg);
	}

	void stack(Error *child) {
		osync_error_stack(&self->error, &child->error);
	}

	%newobject get_child;
	Error *get_child() {
		OSyncError *child = osync_error_get_child(&self->error);
		if (!child)
			return NULL;

		/* Return new Error object that contains a ref'd OSyncError */
		Error *ret = errorwrap_new();
		ret->error = child;
		osync_error_ref(&ret->error);
		return ret;
	}

%pythoncode %{
	# for some reason the OpenSync API only allows setting the msg with a type
	def __set_msg(self, msg):
		self.set(self.num, msg)

	def __str__(self):
		return self.get_name() + ": " + self.get_msg()

	def report(self, context):
		"""Report myself as an error to the given Context object."""
		context.report_osyncerror(self)

	name = property(get_name)
	is_set = property(is_set)
	num = property(get_type, set_type) # 'type' is a reserved word
	msg = property(get_msg, __set_msg)
%}
}

%{
/* If the given opensync error is set, raise a matching exception.
 * Returns TRUE iff an exception was raised. */
static bool
raise_exception_on_error(OSyncError *oserr)
{
	if (!osync_error_is_set(&oserr)) {
		return FALSE;
	}

	PyObject *obj = errorwrap_new_pyobject(oserr);
	PyErr_SetObject(SWIG_Python_ExceptionType(SWIGTYPE_p_Error), obj);
	Py_DECREF(obj);

	return TRUE;
}

static void
wrapper_exception(const char *msg)
{
	OSyncError *err = NULL;
	osync_error_set(&err, OSYNC_ERROR_GENERIC, "internal wrapper error: %s", msg);
	raise_exception_on_error(err);
}
%}


typedef enum {} ErrorType;

/* pull in constants from opensync_error.h without all the functions */
%constant int NO_ERROR = OSYNC_NO_ERROR;
%constant int ERROR_GENERIC = OSYNC_ERROR_GENERIC;
%constant int ERROR_IO_ERROR = OSYNC_ERROR_IO_ERROR;
%constant int ERROR_NOT_SUPPORTED = OSYNC_ERROR_NOT_SUPPORTED;
%constant int ERROR_TIMEOUT = OSYNC_ERROR_TIMEOUT;
%constant int ERROR_DISCONNECTED = OSYNC_ERROR_DISCONNECTED;
%constant int ERROR_FILE_NOT_FOUND = OSYNC_ERROR_FILE_NOT_FOUND;
%constant int ERROR_EXISTS = OSYNC_ERROR_EXISTS;
%constant int ERROR_CONVERT = OSYNC_ERROR_CONVERT;
%constant int ERROR_MISCONFIGURATION = OSYNC_ERROR_MISCONFIGURATION;
%constant int ERROR_INITIALIZATION = OSYNC_ERROR_INITIALIZATION;
%constant int ERROR_PARAMETER = OSYNC_ERROR_PARAMETER;
%constant int ERROR_EXPECTED = OSYNC_ERROR_EXPECTED;
%constant int ERROR_NO_CONNECTION = OSYNC_ERROR_NO_CONNECTION;
%constant int ERROR_TEMPORARY = OSYNC_ERROR_TEMPORARY;
%constant int ERROR_LOCKED = OSYNC_ERROR_LOCKED;
%constant int ERROR_PLUGIN_NOT_FOUND = OSYNC_ERROR_PLUGIN_NOT_FOUND;
