typedef enum {} ConfigurationType;

%constant int PLUGIN_NO_CONFIGURATION = OSYNC_PLUGIN_NO_CONFIGURATION;
%constant int PLUGIN_OPTIONAL_CONFIGURATION = OSYNC_PLUGIN_OPTIONAL_CONFIGURATION;
%constant int PLUGIN_NEEDS_CONFIGURATION = OSYNC_PLUGIN_NEEDS_CONFIGURATION;

typedef enum {} StartType;

%constant int START_TYPE_UNKNOWN = OSYNC_START_TYPE_UNKNOWN;
%constant int START_TYPE_PROCESS = OSYNC_START_TYPE_PROCESS;
%constant int START_TYPE_THREAD = OSYNC_START_TYPE_THREAD;
%constant int START_TYPE_EXTERNAL = OSYNC_START_TYPE_EXTERNAL;

typedef enum {} ConfigSupportedFlag;

%constant int PLUGIN_CONFIG_ADVANCEDOPTION = OSYNC_PLUGIN_CONFIG_ADVANCEDOPTION;
%constant int PLUGIN_CONFIG_AUTHENTICATION = OSYNC_PLUGIN_CONFIG_AUTHENTICATION;
%constant int PLUGIN_CONFIG_LOCALIZATION = OSYNC_PLUGIN_CONFIG_LOCALIZATION;
%constant int PLUGIN_CONFIG_RESOURCES = OSYNC_PLUGIN_CONFIG_RESOURCES;
%constant int PLUGIN_CONFIG_CONNECTION = OSYNC_PLUGIN_CONFIG_CONNECTION;
%constant int PLUGIN_CONFIG_EXTERNALPLUGIN = OSYNC_PLUGIN_CONFIG_EXTERNALPLUGIN;

typedef enum {} AdvancedOptionType;

%constant int PLUGIN_ADVANCEDOPTION_TYPE_NONE = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_NONE;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_BOOL = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_BOOL;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_CHAR = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_CHAR;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_DOUBLE = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_DOUBLE;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_INT = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_INT;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_LONG = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_LONG;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_LONGLONG = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_LONGLONG;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_UINT = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_UINT;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_ULONG = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_ULONG;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_ULONGLONG = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_ULONGLONG;
%constant int PLUGIN_ADVANCEDOPTION_TYPE_STRING = OSYNC_PLUGIN_ADVANCEDOPTION_TYPE_STRING;

typedef enum {} AuthenticationOptionSupportedFlag;

%constant int PLUGIN_AUTHENTICATION_USERNAME = OSYNC_PLUGIN_AUTHENTICATION_USERNAME;
%constant int PLUGIN_AUTHENTICATION_PASSWORD = OSYNC_PLUGIN_AUTHENTICATION_PASSWORD;
%constant int PLUGIN_AUTHENTICATION_REFERENCE = OSYNC_PLUGIN_AUTHENTICATION_REFERENCE;

typedef enum {} LocalizationOptionSupportedFlag;

%constant int PLUGIN_LOCALIZATION_ENCODING = OSYNC_PLUGIN_LOCALIZATION_ENCODING;
%constant int PLUGIN_LOCALIZATION_TIMEZONE = OSYNC_PLUGIN_LOCALIZATION_TIMEZONE;
%constant int PLUGIN_LOCALIZATION_LANGUAGE = OSYNC_PLUGIN_LOCALIZATION_LANGUAGE;

typedef enum {} ConnectionType;

%constant int PLUGIN_CONNECTION_UNKNOWN = OSYNC_PLUGIN_CONNECTION_UNKNOWN;
%constant int PLUGIN_CONNECTION_BLUETOOTH = OSYNC_PLUGIN_CONNECTION_BLUETOOTH;
%constant int PLUGIN_CONNECTION_USB = OSYNC_PLUGIN_CONNECTION_USB;
%constant int PLUGIN_CONNECTION_NETWORK = OSYNC_PLUGIN_CONNECTION_NETWORK;
%constant int PLUGIN_CONNECTION_SERIAL = OSYNC_PLUGIN_CONNECTION_SERIAL;
%constant int PLUGIN_CONNECTION_IRDA = OSYNC_PLUGIN_CONNECTION_IRDA;

typedef enum {} ConnectionSupportedFlag;
typedef unsigned int ConnectionSupportedFlags;

typedef enum {} ConnectionOptionSupportedFlag;

%constant int PLUGIN_CONNECTION_BLUETOOTH_ADDRESS = OSYNC_PLUGIN_CONNECTION_BLUETOOTH_ADDRESS;
%constant int PLUGIN_CONNECTION_BLUETOOTH_RFCOMM = OSYNC_PLUGIN_CONNECTION_BLUETOOTH_RFCOMM;
%constant int PLUGIN_CONNECTION_BLUETOOTH_SDPUUID = OSYNC_PLUGIN_CONNECTION_BLUETOOTH_SDPUUID;
%constant int PLUGIN_CONNECTION_USB_VENDORID = OSYNC_PLUGIN_CONNECTION_USB_VENDORID;
%constant int PLUGIN_CONNECTION_USB_PROUCTID = OSYNC_PLUGIN_CONNECTION_USB_PRODUCTID;
%constant int PLUGIN_CONNECTION_USB_INTERFACE = OSYNC_PLUGIN_CONNECTION_USB_INTERFACE;
%constant int PLUGIN_CONNECTION_NETWORK_ADDRESS = OSYNC_PLUGIN_CONNECTION_NETWORK_ADDRESS;
%constant int PLUGIN_CONNECTION_NETWORK_PORT = OSYNC_PLUGIN_CONNECTION_NETWORK_PORT;
%constant int PLUGIN_CONNECTION_NETWORK_PROTOCOL = OSYNC_PLUGIN_CONNECTION_NETWORK_PROTOCOL;
%constant int PLUGIN_CONNECTION_NETWORK_DNSSD = OSYNC_PLUGIN_CONNECTION_NETWORK_DNSSD;
%constant int PLUGIN_CONNECTION_SERIAL_SPEED = OSYNC_PLUGIN_CONNECTION_SERIAL_SPEED;
%constant int PLUGIN_CONNECTION_SERIAL_DEVICENODE = OSYNC_PLUGIN_CONNECTION_SERIAL_DEVICENODE;
%constant int PLUGIN_CONNECTION_IRDA_SERVICE = OSYNC_PLUGIN_CONNECTION_IRDA_SERVICE;

typedef enum {} ResourceOptionSupportedFlag;

%constant int PLUGIN_RESOURCE_NAME = OSYNC_PLUGIN_RESOURCE_NAME;
%constant int PLUGIN_RESOURCE_PATH = OSYNC_PLUGIN_RESOURCE_PATH;
%constant int PLUGIN_RESOURCE_URL = OSYNC_PLUGIN_RESOURCE_URL;

typedef struct {} Plugin;
%extend Plugin {
	/* called by python-module plugin */
	Plugin(PyObject *obj) {
		Plugin *plugin = PyCObject_AsVoidPtr(obj);
		osync_plugin_ref(plugin);
		return plugin;
	}

	Plugin() {
		OSyncError *err = NULL;
		Plugin *plugin = osync_plugin_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return plugin;
		}

	~Plugin() {
		osync_plugin_unref(self);
	}

	const char *get_name() {
		return osync_plugin_get_name(self);
	}

	void set_name(const char *name) {
		osync_plugin_set_name(self, name);
	}

	const char *get_longname() {
		return osync_plugin_get_longname(self);
	}

	void set_longname(const char *longname) {
		osync_plugin_set_longname(self, longname);
	}

	const char *get_description() {
		return osync_plugin_get_description(self);
	}

	void set_description(const char *description) {
		osync_plugin_set_description(self, description);
	}

	ConfigurationType get_config_type() {
		return osync_plugin_get_config_type(self);
	}

	void set_config_type(ConfigurationType config_type) {
		osync_plugin_set_config_type(self, config_type);
	}

	StartType get_start_type() {
		return osync_plugin_get_start_type(self);
	}

	void set_start_type(StartType start_type) {
		osync_plugin_set_start_type(self, start_type);
	}

	void set_discover_timeout(unsigned int timeout) {
		osync_plugin_set_discover_timeout(self, timeout);
	}

	void set_initialize_timeout(unsigned int timeout) {
		osync_plugin_set_initialize_timeout(self, timeout);
	}

	void set_finalize_timeout(unsigned int timeout) {
		osync_plugin_set_finalize_timeout(self, timeout);
	}

	osync_bool initialize(PluginInfo *info, void **plugin_data) {
		OSyncError *err = NULL;
		osync_bool ret = osync_plugin_initialize(self, plugin_data, info, &err);
		raise_exception_on_error(err);
		return ret;
	}

	void finalize(void *data) {
		osync_plugin_finalize(self, data);
	}

	void discover(void *data, PluginInfo *info) {
		OSyncError *err = NULL;
		bool ret = osync_plugin_discover(self, data, info, &err);
		if (!raise_exception_on_error(err) && !ret)
			wrapper_exception("osync_plugin_discover failed but did not set error code");
	}

%pythoncode %{
	name = property(get_name, set_name)
	longname = property(get_longname, set_longname)
	description = property(get_description, set_description)
	config_type = property(get_config_type, set_config_type)
	start_type = property(get_start_type, set_start_type)
%}
};


typedef struct {} PluginEnv;
%extend PluginEnv {
	PluginEnv() {
		OSyncError *err = NULL;
		PluginEnv *env = osync_plugin_env_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return env;
	}

	~PluginEnv() {
		osync_plugin_env_unref(self);
	}

	void load(const char *path = NULL) {
		OSyncError *err = NULL;
		bool ret = osync_plugin_env_load(self, path, &err);
		if (!raise_exception_on_error(err) && !ret)
			wrapper_exception("osync_plugin_env_load failed but did not set error code");
	}

	void register_plugin(Plugin *plugin) {
		OSyncError *err = NULL;
		osync_plugin_env_register_plugin(self, plugin, &err);
                raise_exception_on_error(err);
	}

	%newobject find_plugin;
	Plugin *find_plugin(const char *name) {
		Plugin *plugin = osync_plugin_env_find_plugin(self, name);
		if (plugin)
			osync_plugin_ref(plugin);
		return plugin;
	}

	int num_plugins() {
		OSyncList *plugins = osync_plugin_env_get_plugins(self);
		unsigned int num = osync_list_length(plugins);
		osync_list_free(plugins);
		return num;
	}

	%newobject nth_plugin;
	Plugin *nth_plugin(int nth) {
		OSyncList *plugins = osync_plugin_env_get_plugins(self);

		Plugin *plugin = (OSyncPlugin*)osync_list_nth_data(plugins, nth);
		if (plugin)
			osync_plugin_ref(plugin);
		osync_list_free(plugins);
		return plugin;
	}

%pythoncode %{
	# extend the SWIG-generated constructor, so that we can setup our list-wrapper classes
	__oldinit = __init__
	def __init__(self, *args):
		self.__oldinit(*args)
		self.plugins = _ListWrapper(self.num_plugins, self.nth_plugin)
%}
};


typedef struct {} PluginInfo;
%extend PluginInfo {
	/* called by python-module plugin */
	PluginInfo(PyObject *obj) {
		PluginInfo *info = PyCObject_AsVoidPtr(obj);
		osync_plugin_info_ref(info);
		return info;
	}

	PluginInfo() {
		OSyncError *err = NULL;
		PluginInfo *info = osync_plugin_info_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return info;
	}

	~PluginInfo() {
		osync_plugin_info_unref(self);
	}

	void set_loop(void *loop) {
		osync_plugin_info_set_loop(self, loop);
	}

	void *get_loop() {
		return osync_plugin_info_get_loop(self);
	}

	void set_config(PluginConfig *config) {
		osync_plugin_info_set_config(self, config);
	}

	%newobject get_config;
	PluginConfig *get_config() {
		PluginConfig *config = osync_plugin_info_get_config(self);
		if (config)
			osync_plugin_config_ref(config);
		return config;
	}

	void set_configdir(const char *configdir) {
		osync_plugin_info_set_configdir(self, configdir);
	}

	%newobject find_objtype;
	ObjTypeSink *find_objtype(const char *name) {
		ObjTypeSink *objtype = osync_plugin_info_find_objtype(self, name);
		if (objtype)
			osync_objtype_sink_ref(objtype);
		return objtype;
	}

	void add_objtype(ObjTypeSink *sink) {
		osync_plugin_info_add_objtype(self, sink);
	}

	unsigned int num_objtype_sinks() {
		OSyncList *objtypesinks = osync_plugin_info_get_objtype_sinks(self);
		unsigned int num = osync_list_length(objtypesinks);
		osync_list_free(objtypesinks);
		return num;
	}

	%newobject nth_objtype;
	ObjTypeSink *nth_objtype(int nth) {
		/* TODO: return a list structure of phython */
		OSyncList *objtypesinks = osync_plugin_info_get_objtype_sinks(self);
		ObjTypeSink *ret = (ObjTypeSink*)osync_list_nth_data(objtypesinks, nth);
		if (ret)
			osync_objtype_sink_ref(ret);
		osync_list_free(objtypesinks);
		return ret;
	}

	%newobject get_main_sink;
	ObjTypeSink *get_main_sink() {
		ObjTypeSink *ret = osync_plugin_info_get_main_sink(self);
		if (ret)
			osync_objtype_sink_ref(ret);
		return ret;
	}

	void set_main_sink(ObjTypeSink *sink) {
		osync_plugin_info_set_main_sink(self, sink);
	}

	%newobject get_format_env;
	FormatEnv *get_format_env() {
		FormatEnv *env = osync_plugin_info_get_format_env(self);
		if (env)
			osync_format_env_ref(env);
		return env;
	}

	void set_format_env(FormatEnv *env) {
		osync_plugin_info_set_format_env(self, env);
	}

	void set_sink(ObjTypeSink *sink) {
		osync_plugin_info_set_sink(self, sink);
	}

	void set_groupname(const char *groupname) {
		osync_plugin_info_set_groupname(self, groupname);
	}

	const char *get_groupname() {
		return osync_plugin_info_get_groupname(self);
	}

	void set_version(Version *version) {
		osync_plugin_info_set_version(self, version);
	}

	%newobject get_version;
	Version *get_version() {
		Version *ret = osync_plugin_info_get_version(self);
		if (ret)
			osync_version_ref(ret);
		return ret;
	}

	void set_capabilities(Capabilities *capabilities) {
		osync_plugin_info_set_capabilities(self, capabilities);
	}

	%newobject get_capabilities;
	Capabilities *get_capabilities() {
		Capabilities *ret = osync_plugin_info_get_capabilities(self);
		if (ret)
			osync_capabilities_ref(ret);
		return ret;
	}

%pythoncode %{
	loop = property(get_loop, set_loop)
	config = property(get_config, set_config)
	main_sink = property(get_main_sink, set_main_sink)
	format_env = property(get_format_env, set_format_env)
	# sink = property(get_sink, set_sink)
	groupname = property(get_groupname, set_groupname)
	version = property(get_version, set_version)
	capabilities = property(get_capabilities, set_capabilities)

	# extend the SWIG-generated constructor, so that we can setup our list-wrapper classes
	__oldinit = __init__
	def __init__(self, *args):
		self.__oldinit(*args)
		self.objtypes = _ListWrapper(self.num_objtype_sinks, self.nth_objtype)
%}
}


typedef struct {} PluginConfig;
%extend PluginConfig {
	/* called by python-module plugin */
	PluginConfig(PyObject *obj) {
		PluginConfig *config = PyCObject_AsVoidPtr(obj);
		osync_plugin_config_ref(config);
		return config;
	}

	PluginConfig() {
		OSyncError *err = NULL;
		PluginConfig *config = osync_plugin_config_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return config;
	}

	~PluginConfig() {
		osync_plugin_config_unref(self);
	}

	void file_load(const char *path) {
		OSyncError *err = NULL;
		bool ret = osync_plugin_config_file_load(self, path, &err);
		if (!ret && !raise_exception_on_error(err))
			wrapper_exception("osync_plugin_config_file_load failed but did not set error code");
	}

	void file_save(const char *path) {
		OSyncError *err = NULL;
		bool ret = osync_plugin_config_file_save(self, path, &err);
		if (!ret && !raise_exception_on_error(err))
			wrapper_exception("osync_plugin_config_file_save failed but did not set error code");
	}

	osync_bool is_supported(ConfigSupportedFlag flag) {
		return osync_plugin_config_is_supported(self, flag);
	}

	void set_supported(ConfigSupportedFlag flag) {
		osync_plugin_config_set_supported(self, flag);
	}

	unsigned int num_advancedoptions() {
		OSyncList *advancedoptions = osync_plugin_config_get_advancedoptions(self);
		unsigned int num = osync_list_length(advancedoptions);
		osync_list_free(advancedoptions);
		return num;
	}

	%newobject nth_advancedoption;
	PluginAdvancedOption *nth_advancedoption(int nth) {
		/* TODO: return a list structure of phython */
		OSyncList *advancedoptions = osync_plugin_config_get_advancedoptions(self);
		PluginAdvancedOption *ret = (OSyncPluginAdvancedOption*)osync_list_nth_data(advancedoptions, nth);
		if (ret)
			osync_plugin_advancedoption_ref(ret);
		osync_list_free(advancedoptions);
		return ret;
	}

	const char *get_advancedoption_value_by_name(const char *name) {
		return osync_plugin_config_get_advancedoption_value_by_name(self, name);
	}

	void add_advancedoption(PluginAdvancedOption *option) {
		osync_plugin_config_add_advancedoption(self, option);
	}

	void remove_advancedoption(PluginAdvancedOption *option) {
		osync_plugin_config_remove_advancedoption(self, option);
	}

	%newobject get_authentication;
	PluginAuthentication *get_authentication() {
		PluginAuthentication *auth = osync_plugin_config_get_authentication(self);
		if (auth)
			osync_plugin_authentication_ref(auth);
                return auth;
	}

	void set_authentication(PluginAuthentication *authentication) {
		osync_plugin_config_set_authentication(self, authentication);
	}

	%newobject get_localization;
	PluginLocalization *get_localization() {
		PluginLocalization *localization = osync_plugin_config_get_localization(self);
		if (localization)
			osync_plugin_localization_ref(localization);
                return localization;
	}

	void set_localization(PluginLocalization *localization) {
		osync_plugin_config_set_localization(self, localization);
	}

	unsigned int num_resources() {
		OSyncList *resources = osync_plugin_config_get_resources(self);
		unsigned int num = osync_list_length(resources);
		osync_list_free(resources);
		return num;
	}

	%newobject nth_resource;
	PluginResource *nth_resource(int nth) {
		/* TODO: return a list structure of phython */
		OSyncList *resources = osync_plugin_config_get_resources(self);
		PluginResource *ret = (OSyncPluginResource*)osync_list_nth_data(resources, nth);
		if (ret)
			osync_plugin_resource_ref(ret);
		osync_list_free(resources);
		return ret;
	}

	%newobject find_active_resource;
	PluginResource *find_active_resource(const char *objtype) {
		PluginResource *ret = osync_plugin_config_find_active_resource(self, objtype);
		if (ret)
			osync_plugin_resource_ref(ret);
		return ret;
	}

	void add_resource(PluginResource *resource) {
		osync_plugin_config_add_resource(self, resource);
	}

	void remove_resource(PluginResource *resource) {
		osync_plugin_config_remove_resource(self, resource);
	}

	void flush_resources() {
		osync_plugin_config_flush_resources(self);
	}

	%newobject get_connection;
	PluginConnection *get_connection() {
		PluginConnection *connection = osync_plugin_config_get_connection(self);
		if (connection)
			osync_plugin_connection_ref(connection);
                return connection;
	}

	void set_connection(PluginConnection *connection) {
		osync_plugin_config_set_connection(self, connection);
	}

	%newobject get_externalplugin;
	PluginExternalPlugin *get_externalplugin() {
		PluginExternalPlugin *externalplugin = osync_plugin_config_get_externalplugin(self);
		if (externalplugin)
			osync_plugin_externalplugin_ref(externalplugin);
                return externalplugin;
	}

	void set_externalplugin(PluginExternalPlugin *externalplugin) {
		osync_plugin_config_set_externalplugin(self, externalplugin);
	}

%pythoncode %{
	authentication = property(get_authentication, set_authentication)
	localization = property(get_localization, set_localization)
	connection = property(get_connection, set_connection)
	externalplugin = property(get_externalplugin, set_externalplugin)

	# extend the SWIG-generated constructor, so that we can setup our list-wrapper classes
	__oldinit = __init__
	def __init__(self, *args):
		self.__oldinit(*args)
		self.advancedoptions = _ListWrapper(self.num_advancedoptions, self.nth_advancedoption)
		self.resources = _ListWrapper(self.num_resources, self.nth_resource)
%}
}


typedef struct {} PluginAdvancedOption;
%extend PluginAdvancedOption {
	PluginAdvancedOption() {
		OSyncError *err = NULL;
		PluginAdvancedOption *option = osync_plugin_advancedoption_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return option;
	}

	~PluginAdvancedOption() {
		osync_plugin_advancedoption_unref(self);
	}

	int num_parameters() {
		OSyncList *params = osync_plugin_advancedoption_get_parameters(self);
		unsigned int num = osync_list_length(params);
		osync_list_free(params);
		return num;
	}

	%newobject nth_parameter;
	PluginAdvancedOptionParameter *nth_parameter(int nth) {
		OSyncList *params = osync_plugin_advancedoption_get_parameters(self);

		PluginAdvancedOptionParameter *param = (OSyncPluginAdvancedOptionParameter*)osync_list_nth_data(params, nth);
		if (param)
			osync_plugin_advancedoption_param_ref(param);
		osync_list_free(params);
		return param;
	}

	void add_parameter(PluginAdvancedOptionParameter *parameter) {
		osync_plugin_advancedoption_add_parameter(self, parameter);
	}

	void remove_parameter(PluginAdvancedOptionParameter *parameter) {
		osync_plugin_advancedoption_remove_parameter(self, parameter);
	}

	unsigned int get_max() {
		return osync_plugin_advancedoption_get_max(self);
	}

	void set_max(unsigned int max) {
		osync_plugin_advancedoption_set_max(self, max);
	}

	unsigned int get_min() {
		return osync_plugin_advancedoption_get_min(self);
	}

	void set_min(unsigned int min) {
		osync_plugin_advancedoption_set_min(self, min);
	}

	unsigned int get_maxoccurs() {
		return osync_plugin_advancedoption_get_maxoccurs(self);
	}

	void set_maxoccurs(unsigned int maxoccurs) {
		osync_plugin_advancedoption_set_maxoccurs(self, maxoccurs);
	}

	const char *get_displayname() {
		return osync_plugin_advancedoption_get_displayname(self);
	}

	void set_displayname(const char *displayname) {
		osync_plugin_advancedoption_set_displayname(self, displayname);
	}

	const char *get_name() {
		return osync_plugin_advancedoption_get_name(self);
	}

	void set_name(const char *name) {
		osync_plugin_advancedoption_set_name(self, name);
	}

	AdvancedOptionType get_type() {
		return osync_plugin_advancedoption_get_type(self);
	}

	const char *get_type_string() {
		return osync_plugin_advancedoption_get_type_string(self);
	}

	void set_type(AdvancedOptionType type) {
		osync_plugin_advancedoption_set_type(self, type);
	}

	int num_valenums() {
		OSyncList *valenums = osync_plugin_advancedoption_get_valenums(self);
		unsigned int num = osync_list_length(valenums);
		osync_list_free(valenums);
		return num;
	}

	const char *nth_valenum(int nth) {
		OSyncList *valenums = osync_plugin_advancedoption_get_valenums(self);
		const char *value = (const char*)osync_list_nth_data(valenums, nth);
		osync_list_free(valenums);
		return value;
	}

	void add_valenum(const char *value) {
		osync_plugin_advancedoption_add_valenum(self, value);
	}

	void remove_valenum(const char *value) {
		osync_plugin_advancedoption_remove_valenum(self, value);
	}

	void set_value(const char *value) {
		osync_plugin_advancedoption_set_value(self, value);
	}

	const char *get_value() {
		return osync_plugin_advancedoption_get_value(self);
	}

%pythoncode %{
	max = property(get_max, set_max)
	min = property(get_min, set_min)
	maxoccurs = property(get_maxoccurs, set_maxoccurs)
	name = property(get_name, set_name)
	displayname = property(get_displayname, set_displayname)
	type = property(get_type, set_type)
	value = property(get_value, set_value)

	# extend the SWIG-generated constructor, so that we can setup our list-wrapper classes
	__oldinit = __init__
	def __init__(self, *args):
		self.__oldinit(*args)
		self.parameters = _ListWrapper(self.num_parameters, self.nth_parameter)
		self.valenums = _ListWrapper(self.num_valenums, self.nth_valenum)
%}
};


typedef struct {} PluginAuthentication;
%extend PluginAuthentication {
	PluginAuthentication() {
		OSyncError *err = NULL;
		PluginAuthentication *auth = osync_plugin_authentication_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return auth;
	}

	~PluginAuthentication() {
		osync_plugin_authentication_unref(self);
	}

	bool option_is_supported(AuthenticationOptionSupportedFlag flag) {
		return osync_plugin_authentication_option_is_supported(self, flag);
	}

	void option_set_supported(AuthenticationOptionSupportedFlag flag) {
		osync_plugin_authentication_option_set_supported(self, flag);
	}

	const char *get_username() {
		return osync_plugin_authentication_get_username(self);
	}

	void set_username(const char *username) {
		osync_plugin_authentication_set_username(self, username);
	}

	const char *get_password() {
		return osync_plugin_authentication_get_password(self);
	}

	void set_password(const char *password) {
		osync_plugin_authentication_set_password(self, password);
	}

	const char *get_reference() {
		return osync_plugin_authentication_get_reference(self);
	}

	void set_reference(const char *reference) {
		osync_plugin_authentication_set_reference(self, reference);
	}
};


typedef struct {} PluginLocalization;
%extend PluginLocalization {
	PluginLocalization() {
		OSyncError *err = NULL;
		PluginLocalization *local = osync_plugin_localization_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return local;
	}

	~PluginLocalization() {
		osync_plugin_localization_unref(self);
	}

	bool option_is_supported(LocalizationOptionSupportedFlag flag) {
		return osync_plugin_localization_option_is_supported(self, flag);
	}

	void option_set_supported(LocalizationOptionSupportedFlag flag) {
		osync_plugin_localization_option_set_supported(self, flag);
	}

	const char *get_encoding() {
		return osync_plugin_localization_get_encoding(self);
	}

	void set_encoding(const char *encoding) {
		osync_plugin_localization_set_encoding(self, encoding);
	}

	const char *get_timezone() {
		return osync_plugin_localization_get_timezone(self);
	}

	void set_timezone(const char *timezone) {
		osync_plugin_localization_set_timezone(self, timezone);
	}

	const char *get_language() {
		return osync_plugin_localization_get_language(self);
	}

	void set_language(const char *language) {
		osync_plugin_localization_set_language(self, language);
	}
};


typedef struct {} PluginAdvancedOptionParameter;
%extend PluginAdvancedOptionParameter {
	PluginAdvancedOptionParameter() {
		OSyncError *err = NULL;
		PluginAdvancedOptionParameter *param = osync_plugin_advancedoption_param_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return param;
	}

	~PluginAdvancedOptionParameter() {
		osync_plugin_advancedoption_param_unref(self);
	}

	const char *get_displayname() {
		return osync_plugin_advancedoption_param_get_displayname(self);
	}

	void set_displayname(const char *displayname) {
		osync_plugin_advancedoption_param_set_displayname(self, displayname);
	}

	const char *get_name() {
		return osync_plugin_advancedoption_param_get_name(self);
	}

	void set_name(const char *name) {
		osync_plugin_advancedoption_param_set_name(self, name);
	}

	AdvancedOptionType get_type() {
		return osync_plugin_advancedoption_param_get_type(self);
	}

	const char *get_type_string() {
		return osync_plugin_advancedoption_param_get_type_string(self);
	}

	void set_type(AdvancedOptionType type) {
		osync_plugin_advancedoption_param_set_type(self, type);
	}

	int num_valenums() {
		OSyncList *valenums = osync_plugin_advancedoption_param_get_valenums(self);
		unsigned int num = osync_list_length(valenums);
		osync_list_free(valenums);
		return num;
	}

	const char *nth_valenum(int nth) {
		OSyncList *valenums = osync_plugin_advancedoption_param_get_valenums(self);
		const char *value = (const char*)osync_list_nth_data(valenums, nth);
		osync_list_free(valenums);
		return value;
	}

	void add_valenum(const char *value) {
		osync_plugin_advancedoption_param_add_valenum(self, value);
	}

	void remove_valenum(const char *value) {
		osync_plugin_advancedoption_param_remove_valenum(self, value);
	}

	void set_value(const char *value) {
		osync_plugin_advancedoption_param_set_value(self, value);
	}

	const char *get_value() {
		return osync_plugin_advancedoption_param_get_value(self);
	}

%pythoncode %{
	name = property(get_name, set_name)
	displayname = property(get_displayname, set_displayname)
	type = property(get_type, set_type)
	value = property(get_value, set_value)

	# extend the SWIG-generated constructor, so that we can setup our list-wrapper classes
	__oldinit = __init__
	def __init__(self, *args):
		self.__oldinit(*args)
		self.valenums = _ListWrapper(self.num_valenums, self.nth_valenum)
%}
};


typedef struct {} PluginConnection;
%extend PluginConnection {
	PluginConnection() {
		OSyncError *err = NULL;
		PluginConnection *connection = osync_plugin_connection_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return connection;
	}

	~PluginConnection() {
		osync_plugin_connection_unref(self);
	}

	ConnectionType get_type() {
		return osync_plugin_connection_get_type(self);
	}

	void set_type(ConnectionType type) {
		osync_plugin_connection_set_type(self, type);
	}

	bool is_supported(ConnectionSupportedFlag flag) {
		return osync_plugin_connection_is_supported(self, flag);
	}

	void set_supported(ConnectionSupportedFlag flag) {
		osync_plugin_connection_set_supported(self, flag);
	}

	bool option_is_supported(ConnectionOptionSupportedFlag flag) {
		return osync_plugin_connection_option_is_supported(self, flag);
	}

	void option_set_supported(ConnectionOptionSupportedFlag flag) {
		osync_plugin_connection_option_set_supported(self, flag);
	}

	const char *bt_get_addr() {
		return osync_plugin_connection_bt_get_addr(self);
	}

	void bt_set_addr(const char *address) {
		osync_plugin_connection_bt_set_addr(self, address);
	}

	unsigned int bt_get_channel() {
		return osync_plugin_connection_bt_get_channel(self);
	}

	void bt_set_channel(unsigned int channel) {
		osync_plugin_connection_bt_set_channel(self, channel);
	}

	const char *bt_get_sdpuuid() {
		return osync_plugin_connection_bt_get_sdpuuid(self);
	}

	void bt_set_sdpuuid(const char *sdpuuid) {
		osync_plugin_connection_bt_set_sdpuuid(self, sdpuuid);
	}

	const char *usb_get_vendorid() {
		return osync_plugin_connection_usb_get_vendorid(self);
	}

	void usb_set_vendorid(const char *vendorid) {
		osync_plugin_connection_usb_set_vendorid(self, vendorid);
	}

	const char *usb_get_productid() {
		return osync_plugin_connection_usb_get_productid(self);
	}

	void usb_set_productid(const char *productid) {
		osync_plugin_connection_usb_set_productid(self, productid);
	}

	unsigned int usb_get_interface() {
		return osync_plugin_connection_usb_get_interface(self);
	}

	void usb_set_interface(unsigned int interf) {
		osync_plugin_connection_usb_set_interface(self, interf);
	}

	const char *net_get_address() {
		return osync_plugin_connection_net_get_address(self);
	}

	void net_set_address(const char *address) {
		osync_plugin_connection_net_set_address(self, address);
	}

	unsigned int net_get_port() {
		return osync_plugin_connection_net_get_port(self);
	}

	void net_set_port(unsigned int port) {
		osync_plugin_connection_net_set_port(self, port);
	}

	const char *net_get_protocol() {
		return osync_plugin_connection_net_get_protocol(self);
	}

	void net_set_protocol(const char *protocol) {
		osync_plugin_connection_net_set_protocol(self, protocol);
	}

	const char *net_get_dnssd() {
		return osync_plugin_connection_net_get_dnssd(self);
	}

	void net_set_dnssd(const char *dnssd) {
		osync_plugin_connection_net_set_dnssd(self, dnssd);
	}

	unsigned int serial_get_speed() {
		return osync_plugin_connection_serial_get_speed(self);
	}

	void serial_set_speed(unsigned int speed) {
		osync_plugin_connection_serial_set_speed(self, speed);
	}

	const char *serial_get_devicenode() {
		return osync_plugin_connection_serial_get_devicenode(self);
	}

	void serial_set_devicenode(const char *devicenode) {
		osync_plugin_connection_serial_set_devicenode(self, devicenode);
	}

	const char *irda_get_service() {
		return osync_plugin_connection_irda_get_service(self);
	}

	void irda_set_service(const char *service) {
		osync_plugin_connection_irda_set_service(self, service);
	}
};


typedef struct {} PluginExternalPlugin;
%extend PluginExternalPlugin {
	PluginExternalPlugin() {
		OSyncError *err = NULL;
		PluginExternalPlugin *external_plugin = osync_plugin_externalplugin_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return external_plugin;
	}

	~PluginExternalPlugin() {
		osync_plugin_externalplugin_unref(self);
	}

	const char *get_external_command() {
		return osync_plugin_externalplugin_get_external_command(self);
	}

	void set_external_command(const char *external_command) {
		osync_plugin_externalplugin_set_external_command(self, external_command);
	}
};


typedef struct {} PluginResource;
%extend PluginResource {
	PluginResource(PyObject *obj) {
		PluginResource *res = PyCObject_AsVoidPtr(obj);
		osync_plugin_resource_ref(res);
		return res;
	}

	PluginResource() {
		OSyncError *err = NULL;
		PluginResource *res = osync_plugin_resource_new(&err);
		if (raise_exception_on_error(err))
			return NULL;
		else
			return res;
	}

	~PluginResource() {
		osync_plugin_resource_unref(self);
	}

	osync_bool option_is_supported(ResourceOptionSupportedFlag flag) {
		return osync_plugin_resource_option_is_supported(self, flag);
	}

	void option_set_supported(unsigned int flags) {
		osync_plugin_resource_option_set_supported(self, flags);
	}

	osync_bool is_enabled() {
		return osync_plugin_resource_is_enabled(self);
	}

	void enable(bool enable) {
		osync_plugin_resource_enable(self, enable);
	}

	const char *get_name() {
		return osync_plugin_resource_get_name(self);
	}

	void set_name(const char *name) {
		osync_plugin_resource_set_name(self, name);
	}

	const char *get_mime() {
		return osync_plugin_resource_get_mime(self);
	}

	void set_mime(const char *mime) {
		osync_plugin_resource_set_mime(self, mime);
	}

	const char *get_preferred_format() {
		return osync_plugin_resource_get_preferred_format(self);
	}

	void set_preferred_format(const char *preferred_format) {
		osync_plugin_resource_set_preferred_format(self, preferred_format);
	}

	unsigned int num_objformatsinks() {
		OSyncList *objformatsinks = osync_plugin_resource_get_objformat_sinks(self);
		unsigned int num = osync_list_length(objformatsinks);
		osync_list_free(objformatsinks);
		return num;
	}

	%newobject nth_objformatsink;
	ObjFormatSink *nth_objformatsink(int nth) {
		OSyncList *objformatsinks = osync_plugin_resource_get_objformat_sinks(self);
		ObjFormatSink *ret = (ObjFormatSink*)osync_list_nth_data(objformatsinks, nth);
		if (ret)
			osync_objformat_sink_ref(ret);
		osync_list_free(objformatsinks);
		return ret;
	}

	void add_objformat_sink(ObjFormatSink *formatsink) {
		osync_plugin_resource_add_objformat_sink(self, formatsink);
	}

	void remove_objformat_sink(ObjFormatSink *formatsink) {
		osync_plugin_resource_remove_objformat_sink(self, formatsink);
	}

	const char *get_objtype() {
		return osync_plugin_resource_get_objtype(self);
	}

	void set_objtype(const char *objtype) {
		osync_plugin_resource_set_objtype(self, objtype);
	}

	const char *get_path() {
		return osync_plugin_resource_get_path(self);
	}

	void set_path(const char *path) {
		osync_plugin_resource_set_path(self, path);
	}

	const char *get_url() {
		return osync_plugin_resource_get_url(self);
	}

	void set_url(const char *url) {
		osync_plugin_resource_set_url(self, url);
	}

%pythoncode %{
	enabled = property(is_enabled, enable)
	name = property(get_name, set_name)
	mime = property(get_mime, set_mime)
	preferred_format = property(get_preferred_format, set_preferred_format)
	objtype = property(get_objtype, set_objtype)
	path = property(get_path, set_path)
	url = property(get_url, set_url)

	# extend the SWIG-generated constructor, so that we can setup our list-wrapper classes
	__oldinit = __init__
	def __init__(self, *args):
		self.__oldinit(*args)
		self.objformats = _ListWrapper(self.num_objformatsinks, self.nth_objformatsink)
%}
}


typedef struct {} ObjTypeSink;
%extend ObjTypeSink {
	/* create new sink object
	 * when using the python-module plugin, the second argument is
	 * the python object that will get callbacks for this sink */
	ObjTypeSink(const char *objtype, PyObject *callback_obj = NULL) {
		OSyncError *err = NULL;
		ObjTypeSink *sink = osync_objtype_sink_new(objtype, &err);
		if (raise_exception_on_error(err))
			return NULL;

		/* set userdata pointer to supplied python wrapper object */
		if (callback_obj) {
			Py_INCREF(callback_obj);
			osync_objtype_sink_set_userdata(sink, callback_obj);
		}

		return sink;
	}

	~ObjTypeSink() {
		PyObject *callback_obj = osync_objtype_sink_get_userdata(self);
		if (callback_obj) {
			osync_objtype_sink_set_userdata(self, NULL);
			Py_DECREF(callback_obj);
		}
		osync_objtype_sink_unref(self);
	}

	/* set a python callback object in userdata
           do we really want to expose this function ? */
	void set_callback_obj(PyObject *callback_obj = NULL) {
		/* first free any pre-existing object */
		PyObject *prev_obj = osync_objtype_sink_get_userdata(self);
		if (prev_obj) {
			osync_objtype_sink_set_userdata(self, NULL);
			Py_DECREF(prev_obj);
		}

		/* set userdata pointer to supplied python wrapper object */
		if (callback_obj) {
			Py_INCREF(callback_obj);
			osync_objtype_sink_set_userdata(self, callback_obj);
		}
	}

	/* as above, but return it as a PyObject * for python code */
	PyObject *get_callback_obj() {
		return osync_objtype_sink_get_userdata(self);
	}

	const char *get_name() {
		return osync_objtype_sink_get_name(self);
	}

	void set_name(const char *name) {
		osync_objtype_sink_set_name(self, name);
	}

	const char *get_preferred_format() {
		return osync_objtype_sink_get_preferred_format(self);
	}

	void set_preferred_format(const char *preferred_format) {
		osync_objtype_sink_set_preferred_format(self, preferred_format);
	}

	%newobject find_objformat_sink;
	ObjFormatSink *find_objformat_sink(ObjFormat *objformat) {
		ObjFormatSink *sink = osync_objtype_sink_find_objformat_sink(self, objformat);
		if (sink)
			osync_objformat_sink_ref(sink);
		return sink;
	}

	void add_objformat_sink(ObjFormatSink *format_sink) {
		osync_objtype_sink_add_objformat_sink(self, format_sink);
	}

	void remove_objformat_sink(ObjFormatSink *format_sink) {
		osync_objtype_sink_remove_objformat_sink(self, format_sink);
	}

	void enable_state_db(bool enable) {
		osync_objtype_sink_enable_state_db(self, enable);
	}

        OSyncSinkStateDB *get_state_db() {
                return osync_objtype_sink_get_state_db(self);
        }

	void enable_hashtable(bool enable) {
		osync_objtype_sink_enable_hashtable(self, enable);
	}

        OSyncHashTable *get_hashtable() {
                return osync_objtype_sink_get_hashtable(self);
        }

	void get_changes(PluginInfo *info, bool slow_sync, Context *ctx) {
		osync_objtype_sink_get_changes(self, info, slow_sync, ctx);
	}

	void read_change(PluginInfo *info, Change *change, Context *ctx) {
		osync_objtype_sink_read_change(self, info, change, ctx);
	}

	void connect(PluginInfo *info, Context *ctx) {
		osync_objtype_sink_connect(self, info, ctx);
	}

	void disconnect(PluginInfo *info, Context *ctx) {
		osync_objtype_sink_disconnect(self, info, ctx);
	}

	void sync_done(PluginInfo *info, Context *ctx) {
		osync_objtype_sink_sync_done(self, info, ctx);
	}

	void connect_done(PluginInfo *info, Context *ctx) {
		osync_objtype_sink_connect_done(self, info, ctx);
	}

	void commit_change(PluginInfo *info, Change *change, Context *ctx) {
		osync_objtype_sink_commit_change(self, info, change, ctx);
	}

	void committed_all(PluginInfo *info, Context *ctx) {
		osync_objtype_sink_committed_all(self, info, ctx);
	}

	bool is_enabled() {
		return osync_objtype_sink_is_enabled(self);
	}

	void set_enabled(bool enabled) {
		osync_objtype_sink_set_enabled(self, enabled);
	}

	bool is_available() {
		return osync_objtype_sink_is_available(self);
	}

	void set_available(bool available) {
		osync_objtype_sink_set_available(self, available);
	}

	bool get_write() {
		return osync_objtype_sink_get_write(self);
	}

	void set_write(bool write) {
		osync_objtype_sink_set_write(self, write);
	}

	bool get_getchanges() {
		return osync_objtype_sink_get_getchanges(self);
	}

	void set_getchanges(bool getchanges) {
		osync_objtype_sink_set_getchanges(self, getchanges);
	}

	bool get_read() {
		return osync_objtype_sink_get_read(self);
	}

	void set_read(bool read) {
		osync_objtype_sink_set_read(self, read);
	}

	void set_connect_timeout(unsigned int timeout) {
		osync_objtype_sink_set_connect_timeout(self, timeout);
	}

	void set_disconnect_timeout(unsigned int timeout) {
		osync_objtype_sink_set_disconnect_timeout(self, timeout);
	}

	void set_getchanges_timeout(unsigned int timeout) {
		osync_objtype_sink_set_getchanges_timeout(self, timeout);
	}

	void set_commit_timeout(unsigned int timeout) {
		osync_objtype_sink_set_commit_timeout(self, timeout);
	}

	void set_committedall_timeout(unsigned int timeout) {
		osync_objtype_sink_set_committedall_timeout(self, timeout);
	}

	void set_syncdone_timeout(unsigned int timeout) {
		osync_objtype_sink_set_syncdone_timeout(self, timeout);
	}

	void set_read_timeout(unsigned int timeout) {
		osync_objtype_sink_set_read_timeout(self, timeout);
	}

	/* TODO: osync_objtype_sink_get_objformat_sinks with OSyncList */

	unsigned int num_objformatsinks() {
		OSyncList *objformatsinks = osync_objtype_sink_get_objformat_sinks(self);
		unsigned int num = osync_list_length(objformatsinks);
		osync_list_free(objformatsinks);
		return num;
	}

	%newobject nth_objformatsink;
	ObjFormatSink *nth_objformatsink(int nth) {
		OSyncList *objformatsinks = osync_objtype_sink_get_objformat_sinks(self);
		ObjFormatSink *ret = (ObjFormatSink*)osync_list_nth_data(objformatsinks, nth);
		if (ret)
			osync_objformat_sink_ref(ret);
		osync_list_free(objformatsinks);
		return ret;
	}

	/* returns a list of strings */
/*
	PyObject *get_objformats() {
		const OSyncList *list = osync_objtype_sink_get_objformats(self);
		return osynclist_to_pylist(list, SWIGTYPE_p_char);
	}
*/

%pythoncode %{
	name = property(get_name, set_name)
	preferred_format = property(get_preferred_format, set_preferred_format)
	enabled = property(is_enabled, set_enabled)
	available = property(is_available, set_available)
	write = property(get_write, set_write)
	read = property(get_read, set_read)
	#slowsync = property(get_slowsync, set_slowsync)
	#callback_obj = property(get_callback_obj)

	# extend the SWIG-generated constructor, so that we can setup our list-wrapper classes
	__oldinit = __init__
	def __init__(self, *args):
		self.__oldinit(*args)
		self.objformatsinks = _ListWrapper(self.num_objformatsinks, self.nth_objformatsink)
%}
}

%pythoncode %{
class ObjTypeSinkCallbacks:
	"""A purely-Python class that should be subclassed by plugins implementing their own sinks."""
	def __init__(self, objtype=None, objtypesink=None):
		if objtypesink != None:
			# sink exists, just ref it and set ourself as the callback object
			self.sink = objtypesink
			self.sink.set_callback_obj(self)
		else:
			# construct ObjTypeSink object and pass it our reference
			# no objtype implies creation of a Main sink
			self.sink = ObjTypeSink(objtype, self)

	def connect(self, info, ctx):
		pass

	def get_changes(self, info, ctx, slow_sync):
		pass

	def commit(self, info, ctx, chg):
		pass

	def committed_all(self, info, ctx):
		pass

	def read(self, info, ctx, chg):
		pass

	def disconnect(self, info, ctx):
		pass

	def sync_done(self, info, ctx):
		pass

	def connect_done(self, info, ctx, slow_sync):
		pass
%}
